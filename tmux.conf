# use UTF8
set -g utf8
set-window-option -g utf8 on

# make tmux display in 256 colors
set -g default-terminal "screen-256color"

# set scrollback history to 10k
set -g history-limit 10000

# set C-a as default prefix key combo
# and unbind C-b to free it up
set -g prefix C-a
unbind C-b

# use send-prefix to pass C-a through to application
bind C-a send-prefix

# shorten command delay
set -sg escape-time 1

# set window and pane index to 1
set-option -g base-index 1
setw -g pane-base-index 1

# reload ~/.tmux.conf using PREFIX f
bind r source-file ~/.tmux.conf \; display "Reloaded!"

# use prefix | to split windows horizontally and prefix - to split vertically
bind | split-window -h
bind - split-window -v

# make the current window the first window
bind T swap-window -t 1

# map vi movement keys as pane movement keys
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# use C-h and C-l to cycle through panes
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# resize panes using PREFIX H, J, K, L
bind H resize-pane -L 5
bind J resize-pane -D 5
bind K resize-pane -U 5
bind L resize-pane -R 5

# explicitly disable mouse control
setw -g mode-mouse off
set -g mouse-select-pane off
set -g mouse-resize-pane off
set -g mouse-select-window off

# copy and paste
# provide access to clipboard for pbpaste, pbcopy
set-option -g default-command "reattach-to-user-namespace -l zsh"
set-window-option -g automatic-rename on

# use vim keybindings in copy mode
set -g mode-keys vi

# setup 'v' to begin selection as in Vim
bind-key -t vi-copy v begin-selection
bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# update default binding of 'Enter' to also use copy-pipe
unbind -t vi-copy Enter
bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

bind y run 'tmux save-buffer - | reattach-to-user-namespace pbcopy '
bind C-y run 'tmux save-buffer - | reattack-to-user-namespace pbcopy '

# sets colors
set-option -g pane-border-fg colour235
set-option -g pane-active-border-fg colour240

# colorize messages in the command line
set-option -g message-bg black
set-option -g message-fg brightred

# status bar
set-option -g status on
set -g status-utf8 on
set -g status-interval 5
set -g status-justify centre

# visual notifications of activity in other window
setw -g monitor-activity on
set -g visual-activity on

# set color for status bar
set-option -g status-bg colour235
set-option -g status-fg yellow
set-option -g status-attr dim

# set window list colors - red for active and cyan for inactive
set-window-option -g window-status-fg brightblue
set-window-option -g window-status-bg colour236
set-window-option -g window-status-attr dim
set-window-option -g window-status-current-fg brightred
set-window-option -g window-status-current-bg colour236
set-window-option -g window-status-current-attr bright

# show session name, window & pane number, date and time on right side of
# status bar
set -g status-right-length 60
set -g status-right "#[fg=blue]#S #I:#P #[fg=yellow] - %d %b %Y #[fg=green] - %l:%M"
