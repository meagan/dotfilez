# dotfilezZz

if you need dotfiles you might like mine. 

## get 'em

1. gotta use zsh as your shell
` $ chsh -s /bin/zsh`

2. clone 'em
3. install [rcm](https://github.com/thoughtbot/rcm) to create symlinky goodness
   ````
   $ brew tap thoughtbot/rcm
   $ brew install rcm
   ````

4. create those symlinks
   ````
   $ cd ~/
   $ rcup -d dotfilez -x README.md
   ````


you can make them yours (or don't, idc), they're p organized (largely thanks to
[zach](https://github.com/zachmokahn/) for serving as the inspiration for
getting this shit tidy). if you wanna add things just add them in the dotfilez
directory with their category appended to it.

i.e,

- `~/dotfilez/aliases/aliases.something`

now in `~/dotfilez/aliases/aliases` just go right ahead and add that shit

````
" aliases
source ~/.aliases/something.something
````

your shit is symlinked so you only need to rerun rcm if you add new shit
````
$ cd ~/
$ rcup -d dotfilez -x README.md
````

pretty cool.
