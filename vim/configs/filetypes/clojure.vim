" Sets hiccup, clojurescript, and cljx as clojure filetypes
autocmd BufNewFile,BufRead *.hiccup set filetype=clojure
autocmd BufNewFile,BufRead *.cljs set filetype=clojure
autocmd BufNewFile,BufRead *.cljx set filetype=clojure

" Sets some common words found in speclj to fix improper indentation
autocmd FileType clojure setlocal lispwords+=describe,it,context,around,should=,should-not=,should-not,should,should-be,with,run-specs,fresh
