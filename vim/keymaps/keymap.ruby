" vim-rspec mappings
nnoremap <Leader>rcs :call RunCurrentSpecFile()<CR>
nnoremap <Leader>rns :call RunNearestSpec()<CR>
nnoremap <Leader>rls :call RunLastSpec()<CR>
