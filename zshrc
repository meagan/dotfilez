source ~/.zsh/config
source ~/.aliases/aliases
PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
export GOPATH=/usr/local/bin/go
export PATH=$PATH:$GOPATH/bin
export KERLPATH=/usr/local/bin/kerl
export PATH=$PATH:$KERLPATH/bin
